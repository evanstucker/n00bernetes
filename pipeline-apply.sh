#!/bin/bash

if [[ $# -ne 3 ]]; then
  cat <<END >&2

ERROR: You need to set a release, app, and helm repo in that order, for example:

./pipeline-apply.sh tara metamaskapi n00bernetes

or if you're using a public help chart, probably something like:

./pipeline-apply.sh jimmy cerebro stable

END
  exit 1
fi

release="$1"
app="$2"
helm_repo="$3"

source custom_env.sh && \
source static_env.sh && \
helm repo update && \
helm fetch --untar --untardir "charts/${release}" "${helm_repo}/${app}" && \
# Maybe I should delete release here? What could go wrong?
#rm -rf "manifests/${release}"
mkdir -p "manifests/${release}" && \
helm template "charts/${release}/${app}" \
  --name "${release}" \
  --output-dir "manifests/${release}" \
  --values <(cat values/${app}.values.yaml | envsubst) && \
# something here to seal secrets?
kubectl config use-context "$NAME" && \
kubectl apply -R -f "manifests/${release}/${app}" --record
