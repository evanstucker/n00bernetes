#!/bin/bash
# Copied and altered from https://github.com/int128/kubelogin#setup-script

kubectl config set-cluster "$NAME" \
  --embed-certs \
  --server "https://api.${NAME}" \
  --certificate-authority "${NAME}.crt"
kubectl config set-credentials $NAME \
  --auth-provider oidc \
  --auth-provider-arg "idp-issuer-url=https://keycloak.${NAME}/auth/realms/master" \
  --auth-provider-arg "client-id=kubernetes" \
  --auth-provider-arg "client-secret=${client_secret}"
kubectl config set-context "$NAME" --cluster "$NAME" --user "$NAME"
kubectl config use-context "$NAME"
