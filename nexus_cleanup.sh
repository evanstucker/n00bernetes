#!/bin/bash

PATH='/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin'
 
function usage {
cat <<EOF
$(basename $0) gathers a list of container images that are currently in use in your Kubernetes cluster(s), gathers a list of Docker images that are in your Sonatype Nexus repository, compares the lists, and deletes images in your Nexus repo that are not in use.
 
Usage:
 
  $(basename $0) [-h]
 
  -c  Kubernetes clusters (use the context names in your ~/.kube/config). This
      needs to be a quoted, space-delimited list.

  -h  Help

  -n  No gather. Don't look at Kubernetes or Nexus - just use existing txt
      files. This is a troubleshooting feature toggle.

  -p  Sonatype Nexus password

  -r  Sonatype Nexus Docker registry host name

  -t  Safety threshold. If the number of images found in Kubernetes is below
      this number, don't delete anything. This is a sanity check to prevent
      excessive deletion if there's a problem connecting to Kubernetes.

  -u  Sonatype Nexus user name
 
Examples:
  
    This would look at the Kubernetes clusters dev.n00bernetes.com and
    prod.n00bernetes.com, and delete all images not currently in use in those
    clusters from registry.n00bernetes.com.

    $(basename $0) -c 'dev.n00bernetes.com prod.n00bernetes.com' -p BangladeshIsInAsia63 -r registry.n00bernetes.com -u admin
 
EOF
exit 1
}
 
while getopts :c:hnp:r:t:u: option; do
    case $option in
        c) clusters=$OPTARG;;
        h) usage;;
        n) gather=false;;
        p) nexus_pass=$OPTARG;; 
        r) registry=$OPTARG;;
        t) threshold=$OPTARG;;
        u) nexus_user=$OPTARG;;
        \?) echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
        *) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done
 
if [[ $# -eq 0 ]]; then
  usage
fi

export curl_args=(-s -u "${nexus_user}:${nexus_pass}")

if $gather; then
  echo "INFO: Gathering all images in use at these clusters: ${clusters}."
  for cluster in $clusters; do
    kubectl config use-context $cluster &> /dev/null
    kubectl get pods -o json | jq -r '.items[].spec.containers[].image'
  done | grep -F "$registry" | sort -u > images_in_use.txt
  echo "INFO: Found $(cat images_in_use.txt | wc -l) images in use."
fi
if [[ $(cat images_in_use.txt | wc -l) -lt $threshold ]]; then
  echo "ERROR: images_in_use.txt seems too small" >&2; exit 1
else
  if $gather; then
    echo "INFO: Gathering all images in the ${registry} Docker repository..."
    for image in $(curl "${curl_args[@]}" "https://${registry}/v2/_catalog" | jq -r .repositories[]); do
      for tag in $(curl "${curl_args[@]}" "https://${registry}/v2/${image}/tags/list" | jq -r .tags[]); do
        echo "${registry}/${image}:${tag}"
      done
    done | sort > images_in_nexus.txt
    echo "INFO: Found $(cat images_in_nexus.txt | wc -l) images in the repo."
  fi
  echo "INFO: Found $(sort images_in_nexus.txt images_in_use.txt | uniq -u | wc -l) images to delete."
  echo 'INFO: Deleting unused images...'
  for image in $(sort images_in_nexus.txt images_in_use.txt | uniq -u); do
    export name=$(echo $image | cut -d'/' -f2 | cut -d: -f1)
    export reference=$(echo $image | cut -d'/' -f2 | cut -d: -f2)
    export digest=$(curl "${curl_args[@]}" -D - -H 'Accept: application/vnd.docker.distribution.manifest.v2+json' "https://${registry}/v2/${name}/manifests/${reference}" | sed -nE 's/^docker-content-digest: (.*)\r/\1/p')
    echo "INFO: Deleting https://${registry}/v2/${name}/manifests/${digest}..."
    curl "${curl_args[@]}" -D - -X DELETE "https://${registry}/v2/${name}/manifests/${digest}"
  done
fi
echo "Don't forget to run the \"Docker - Delete unused manifests and images\" and \"Admin - Compact blob store\" on ${registry} when this is done to reclaim space."
