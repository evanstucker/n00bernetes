# https://github.com/kubernetes/kops/blob/master/docs/aws.md#cluster-state-storage
variable "kops_bucket" {}
resource "aws_s3_bucket" "kops_bucket" {
  bucket = "${var.kops_bucket}"
  acl    = "private"
  versioning {
    enabled = true
  }
}
