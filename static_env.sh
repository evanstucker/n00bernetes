#!/bin/bash
export TF_VAR_kops_bucket="${AWS_PROFILE/./-}-kops"
export KOPS_STATE_STORE="s3://${TF_VAR_kops_bucket}"
export TF_VAR_terraform_bucket="${AWS_PROFILE/./-}-tf"
export TF_VAR_cluster_domain="${NAME}"
